const HtmlWebpackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const HtmlWebpackTagsPlugin = require("html-webpack-tags-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

const mode = process.env.NODE_ENV || "production";

const port = 3012;
const publicPath = `http://localhost:${port}/`;

const remoteHosts = [];

module.exports = {
  mode,
  entry: "./src/index",
  output: {
    publicPath,
    path: path.resolve(process.cwd(), 'dist'),
  },
  devtool: "source-map",
  devServer: {
    port,
    contentBase: "./dist",
    historyApiFallback: {
      index: "index.html",
    },
  },
  // optimization: {
  //   minimize: mode === "production",
  // },
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.mjs', '.js', '.json', '.tsx', '.ts', '.jsx'],
    // alias: {
    //   '@app/UI': path.resolve(__dirname, "../application-ui/")
    // }
  },
  module: {
    rules: [
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin(),
    new ModuleFederationPlugin({
      name: "appPage1",
      library: { type: "var", name: "appPage1" },
      filename: "remoteEntry.js",
      exposes: {
        "./store": './src/store/root.store',
        "./PageRemote": "./src/app", // This will be make the application-b available as remote
      },
      shared: require("./package.json").dependencies,
    }),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
    }),

    // load the other apps entry
    new HtmlWebpackTagsPlugin({
      tags: remoteHosts.map(remoteHost => `${remoteHost}/remoteEntry.js`),
      append: false, // prepend this as needs to be loaded before application-home
      publicPath: false,
    }),
  ],
};
