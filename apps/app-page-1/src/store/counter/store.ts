import { CounterStoreModel } from './model';

export const defaultStore = {
  count: 0,
};

export const counterStore = CounterStoreModel.create(defaultStore);
