import { types } from 'mobx-state-tree';

export const CounterStoreModel = types
  .model({
    count: 0
  })
  .actions(self => ({
    inc() {
      self.count += 1;
    },
    dec() {
      self.count -= 1;
    },
  }));
