import { types } from 'mobx-state-tree';
import { CounterStoreModel } from './counter/model';
import { counterStore } from './counter/store';

export const RootStoreModel = types
  .model('Page1RootStore', {
    counter: CounterStoreModel
  })


export const createStore = () => {
  return RootStoreModel.create({
    counter: counterStore
  })
}
