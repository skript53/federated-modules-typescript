import React from "react";
import { Counter } from './Counter';
import { Cities } from './Cities';

type Props = { callback?: () => void };


const Page1: React.FC<Props> = ({ callback }) => {
  return (
          <>
            <Counter />
            <Cities />
          </>
  );
};

export default Page1;
