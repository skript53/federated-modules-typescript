import React from 'react'
import { Button } from '@package/ui';
import { observer } from 'mobx-react';

import { useStore } from '@package/store'


export const Counter = observer(() => {
  const { first } = useStore()

  return (
    <p style={{ background: "#cfc", padding: "1em" }}>
      Page1 HELLO
      <h2>{first.counter.count}</h2>
      <div style={{
        margin: '20px'
      }}>
        <button onClick={() => first.counter.inc()} style={{
          padding: '6px'
        }}>
          Плюс
        </button>
        <button onClick={() => first.counter.dec()} style={{
          padding: '6px'
        }}>
          Минус
        </button>
      </div>
      {
        <>
          <br />

          <Button onClick={() => alert('ok')} variant={'container'}>
            Styled Button
          </Button>

        </>
      }
    </p>
  )
})
