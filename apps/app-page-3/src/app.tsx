import React from "react";
import styled from "styled-components";
import { Button } from '@package/ui';

type Props = { callback?: () => void };

const StyledButton = styled.button`
  visibility: visible;
  pointer-events: auto;
  background-color: #ff8080;
  border: 0;
  outline: 0;
  margin: 10px;
  padding: 12px;
  cursor: pointer;
  color: #fff;
  
  &:hover {
    background-color: #d64727;
  }
`

const Cart: React.FC<Props> = ({ callback }) => {
  return (
    <p style={{ background: "#cfc", padding: "1em" }}>
      PAGE 3
      <br />
      <StyledButton>StyledButton</StyledButton>

      <br/>
      <Button variant={'container'}>
        Button UI
      </Button>
    </p>
  );
};

export default Cart;
