import React from "react";
import ReactDOM from "react-dom";

import App from "./app";

ReactDOM.render(
  <>
    <h1>🕹️ PAGE 3</h1>
    <App />
  </>,
  document.getElementById("root")
);
