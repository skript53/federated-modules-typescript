const HtmlWebpackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

const mode = process.env.NODE_ENV || "production";

const port = 3011;
const publicPath = `http://localhost:${port}/`;

module.exports = {
  mode,
  entry: "./src/index",
  output: {
    publicPath,
    path: path.resolve(process.cwd(), 'dist'),
  },
  devtool: "source-map",
  devServer: {
    port,
    contentBase: "./dist",
    historyApiFallback: {
      index: "index.html",
    },
  },
  optimization: {
    minimize: mode === "production",
  },
  resolve: {
    extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: [/node_modules/],
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin(),
    new ModuleFederationPlugin({
      name: "appHome",
      library: { type: "var", name: "appHome" },
      filename: "remoteEntry.js",
      remotes: {},
      exposes: {
        "./HomeRemote": "./src/app"
      },
      shared: require("./package.json").dependencies,
    }),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
    }),
  ],
};
