import React from 'react';
import { observer } from 'mobx-react';
import { useQuery } from '@package/network';

export const Jobs: React.FC = observer(() => {
  const { loading, data } = useQuery(store => store.queryJobs({}));

  if (loading) {
    return <p>loading...</p>;
  }

  return (
    <div
      style={{
        maxHeight: '400px',
        padding: '20px',
        overflowY: 'scroll',
        border: '1px solid black',
        boxShadow: 'inset 0px -21px 24px -15px rgba(0, 0, 0, 0.4)',
      }}
    >
      {data.jobs.map(job => (
        <div>
          <p>{job.title}</p>
        </div>
      ))}
    </div>
  );
});
