import React from 'react';

const Home = React.lazy(() => import('appHome/HomeRemote'));

const Page1 = React.lazy(() => import('appPage1/PageRemote'));
const Page2 = React.lazy(() => import('appPage2/PageRemote'));
const Page3 = React.lazy(() => import('appPage3/PageRemote'));

import { Routes } from '../../common/routes';

export const routeConfig = [
  {
    path: Routes.Home,
    component: Home,
  },
  {
    path: Routes.First,
    component: Page1,
    exact: true,
  },
  {
    path: Routes.Second,
    component: Page2,
    exact: true,
  },
  {
    path: Routes.Third,
    component: Page3,
    exact: true,
  },
];
