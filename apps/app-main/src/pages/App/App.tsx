import React from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';

import { Routes } from '../../common/routes';
import { Jobs } from './components/Jobs';
import { useStore } from '@package/store';


const App = observer<React.FC>(params => {
  const { first } = useStore()

  return (
    <div className='app-shell'>
      <div className='app-shell-nav'>
        <h1>App Shell</h1>
        <Jobs />
        <h1>Счетчик:</h1>
        <h2>{first.counter.count}</h2>
        <div
          style={{
            margin: '20px',
          }}
        >
          <button
            onClick={() => first.counter.inc()}
            style={{
              padding: '6px',
            }}
          >
            Плюс
          </button>
          <button
            onClick={() => first.counter.dec()}
            style={{
              padding: '6px',
            }}
          >
            Минус
          </button>
        </div>
        <nav>
          <Link to={Routes.Home}>Home</Link>
          <Link to={Routes.First}>Page 1</Link>
          <Link to={Routes.Second}>Page 2</Link>
          <Link to={Routes.Third}>Page 3</Link>
        </nav>
      </div>
      <div className='app-shell-main-content'>{params.children}</div>
    </div>
  );
});

export default App;
