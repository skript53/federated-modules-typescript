import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import makeInspectable from 'mobx-devtools-mst';

import { gqlStore, MSTProvider } from './store/mst';
import { RootStoreProvider, rootStore } from './store/root';
import { routeConfig } from './pages/App/App.routing';
import App from './pages/App/App';
import Loading from './common/components/Loading/Loading';

ReactDOM.render(
  <MSTProvider>
    <RootStoreProvider>
      <Router>
        <App>
          <Suspense fallback={<Loading />}>
            <Switch>
              {routeConfig.map(route => (
                <Route {...route} />
              ))}
            </Switch>
          </Suspense>
        </App>
      </Router>
    </RootStoreProvider>
  </MSTProvider>,
  document.getElementById('root')
);

makeInspectable(gqlStore);
makeInspectable(rootStore);
