import { createHttpClient } from 'mst-gql';
import React from 'react';

import { RootStore, StoreContext } from '@package/network';

const defaultStore = undefined;

export const gqlStore = RootStore.create(defaultStore, {
  gqlHttpClient: createHttpClient('https://api.graphql.jobs/'),
});

export const MSTProvider: React.FC = ({ children }) => {
  return <StoreContext.Provider value={gqlStore}>{children}</StoreContext.Provider>;
};
