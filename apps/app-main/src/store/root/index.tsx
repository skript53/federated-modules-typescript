import React from 'react';
import { types } from 'mobx-state-tree';
import { RootStoreContext } from '@package/store';

import { createStore, RootStoreModel as FirstPageStore } from 'appPage1/store';

const RootStoreModel = types.model('RootStore', {
  first: FirstPageStore,
});

export const rootStore = RootStoreModel.create({
  first: createStore()
});

export const RootStoreProvider: React.FC = ({ children }) => {
  return <RootStoreContext.Provider value={rootStore}>{children}</RootStoreContext.Provider>;
};
