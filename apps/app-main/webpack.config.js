const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const HtmlWebpackTagsPlugin = require('html-webpack-tags-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

const mode = process.env.NODE_ENV || 'production';

const port = 3010;
const publicPath = `http://localhost:${port}/`;

const remoteHosts = [
  'http://localhost:3011',
  'http://localhost:3012',
  'http://localhost:3013',
  'http://localhost:3014',
];

module.exports = {
  mode,
  entry: './src/index',
  output: {
    publicPath,
    path: path.resolve(process.cwd(), 'dist'),
  },
  devtool: 'source-map',
  devServer: {
    port,
    contentBase: './dist',
    historyApiFallback: {
      index: 'index.html',
    },
  },
  // optimization: {
  //   minimize: mode === 'production',
  // },
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.mjs', '.js', '.json', '.tsx', '.ts', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin(),
    new ModuleFederationPlugin({
      name: 'appMain',
      library: { type: 'var', name: 'appMain' },
      filename: 'app-main.js', // expose it as `app-shell.js
      remotes: {
        appHome: 'appHome', // loads Home app as remote
        appPage1: 'appPage1', // loads Page 1 app as remote
        appPage2: 'appPage2', // loads Page 2 app as remote
        appPage3: 'appPage3', // loads Page 2 app as remote
      },
      shared: {
        react: { singleton: true, eager: true, requiredVersion: '16.14.0' },
        'react-dom': { singleton: true, eager: true, requiredVersion: '16.14.0' },
        '@package/network': {
          singleton: true,
          eager: true,
          requiredVersion: '1.0.0',
        },
        '@package/store': {
          singleton: true,
          eager: true,
          requiredVersion: '1.0.0',
        },
      },
    }),
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),

    // load the other apps entry
    new HtmlWebpackTagsPlugin({
      tags: remoteHosts.map(remoteHost => `${remoteHost}/remoteEntry.js`),
      append: false, // prepend this as needs to be loaded before application-home
      publicPath: false,
    }),
  ],
};
