import React from "react";
import styled from "styled-components";

type Props = { callback?: () => void };

const StyledButton = styled.button`
  visibility: visible;
  pointer-events: auto;
  background-color: #f8e86c;
  border: 0;
  outline: 0;
  margin: 10px;
  padding: 12px;
  cursor: pointer;
  color: #242424;

  &:hover {
    background-color: #ffe300;
  }
`

const Cart: React.FC<Props> = ({ callback }) => {
  return (
    <p style={{ background: "#fffcda", padding: "1em" }}>
      PAGE 2
      <br />
      <StyledButton>Styled Button</StyledButton>
    </p>
  );
};

export default Cart;
