const HtmlWebpackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

const mode = process.env.NODE_ENV || "production";

const port = 3013;
const publicPath = `http://localhost:${port}/`;

module.exports = {
  mode,
  entry: "./src/index",
  output: {
    publicPath,
    path: path.resolve(process.cwd(), 'dist'),
  },
  devtool: "source-map",
  devServer: {
    port,
    contentBase: "./dist",
    historyApiFallback: {
      index: "index.html",
    },
  },
  optimization: {
    minimize: mode === "production",
  },
  resolve: {
    extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin(),
    new ModuleFederationPlugin({
      name: "appPage2",
      library: { type: "var", name: "appPage2" },
      filename: "remoteEntry.js",
      exposes: {
        "./PageRemote": "./src/app", // This will be make the application-b available as remote
      },
      shared: require("./package.json").dependencies,
    }),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
    }),
  ],
};
