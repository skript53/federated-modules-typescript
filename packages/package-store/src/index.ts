import { createContext, useContext } from 'react';
import { Instance } from 'mobx-state-tree'

type RootStore = Record<string, Instance<any>>

export const RootStoreContext = createContext<RootStore>({});

export const useStore = () => {
  return useContext(RootStoreContext);
};
