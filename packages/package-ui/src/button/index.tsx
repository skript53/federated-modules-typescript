import React from "react";
import styled from "styled-components";

type ButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  variant: 'container' | 'text' | 'outlined'
};

const StyledButton = styled.button`
  visibility: visible;
  pointer-events: auto;
  background-color: #e6e6e6;
  border: 0;
  outline: 0;
  margin: 0;
  padding: 12px;
`

export const Button: React.FC<ButtonProps> = ({ variant, ...propsNative }) => {
  return (
    <StyledButton {...propsNative}>
     PAGE 2
    </StyledButton>
  );
};
