const HtmlWebpackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const HtmlWebpackTagsPlugin = require("html-webpack-tags-plugin");
const path   = require("path");

const mode = process.env.NODE_ENV || "production";

module.exports = {
  mode,
  entry: "./src/index",
  output: {
    publicPath: '/',
    path: path.resolve(__dirname, './dist'),
    filename: 'applicationUI.js',
    libraryTarget: 'umd',
    globalObject: 'this',
    umdNamedDefine: true,
    library: 'applicationUI',
  },
  devtool: "source-map",
  optimization: {
    minimize: mode === "production",
  },
  resolve: {
    extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },

  // plugins: [
  //   new ModuleFederationPlugin({
  //     name: "applicationUI",
  //     library: { type: "var", name: "applicationUI" },
  //     // filename: "remoteEntry.js",
  //     exposes: {},
  //     shared: ["react"],
  //   }),
  // ],
};
