const path = require("path");

const mode = process.env.NODE_ENV || "production";

module.exports = {
  mode,
  entry: "./src/index",
  output: {
    publicPath: '/',
    path: path.resolve(__dirname, './dist'),
    filename: 'applicationNetwork.js',
    libraryTarget: 'umd',
    globalObject: 'this',
    umdNamedDefine: true,
    library: 'applicationNetwork',
  },
  devtool: "source-map",
  optimization: {
    minimize: mode === "production",
  },
  resolve: {
    extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
};
