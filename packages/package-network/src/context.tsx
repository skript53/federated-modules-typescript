import React, { useContext } from 'react';

import { StoreContext } from './models';

export const useMSTStore = () => {
  return useContext(StoreContext)
}
