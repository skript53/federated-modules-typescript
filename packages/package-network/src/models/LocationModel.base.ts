/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */

import { types } from "mobx-state-tree"
import { QueryBuilder } from "mst-gql"
import { ModelBase } from "./ModelBase"
import { RootStoreType } from "./index"


/**
 * LocationBase
 * auto generated base class for the model LocationModel.
 */
export const LocationModelBase = ModelBase
  .named('Location')
  .props({
    __typename: types.optional(types.literal("Location"), "Location"),
    id: types.identifier,
    slug: types.union(types.undefined, types.string),
    name: types.union(types.undefined, types.string),
    type: types.union(types.undefined, types.string),
  })
  .views(self => ({
    get store() {
      return self.__getStore<RootStoreType>()
    }
  }))

export class LocationModelSelector extends QueryBuilder {
  get id() { return this.__attr(`id`) }
  get slug() { return this.__attr(`slug`) }
  get name() { return this.__attr(`name`) }
  get type() { return this.__attr(`type`) }
}
export function selectFromLocation() {
  return new LocationModelSelector()
}

export const locationModelPrimitives = selectFromLocation().slug.name.type
