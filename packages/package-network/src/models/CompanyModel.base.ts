/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */

import { IObservableArray } from "mobx"
import { types } from "mobx-state-tree"
import { MSTGQLRef, QueryBuilder, withTypedRefs } from "mst-gql"
import { ModelBase } from "./ModelBase"
import { JobModel, JobModelType } from "./JobModel"
import { JobModelSelector } from "./JobModel.base"
import { RootStoreType } from "./index"


/* The TypeScript type that explicits the refs to other models in order to prevent a circular refs issue */
type Refs = {
  jobs: IObservableArray<JobModelType>;
}

// @ts-ignore
/**
 * CompanyBase
 * auto generated base class for the model CompanyModel.
 */
export const CompanyModelBase = withTypedRefs<Refs>()(ModelBase
  .named('Company')
  .props({
    __typename: types.optional(types.literal("Company"), "Company"),
    id: types.identifier,
    name: types.union(types.undefined, types.string),
    slug: types.union(types.undefined, types.string),
    websiteUrl: types.union(types.undefined, types.string),
    logoUrl: types.union(types.undefined, types.null, types.string),
    jobs: types.union(types.undefined, types.null, types.array(MSTGQLRef(types.late((): any => JobModel)))),
    twitter: types.union(types.undefined, types.null, types.string),
    emailed: types.union(types.undefined, types.null, types.boolean),
    createdAt: types.union(types.undefined, types.frozen()),
    updatedAt: types.union(types.undefined, types.frozen()),
  })
  .views(self => ({
    get store() {
      return self.__getStore<RootStoreType>()
    }
  })))

export class CompanyModelSelector extends QueryBuilder {
  get id() { return this.__attr(`id`) }
  get name() { return this.__attr(`name`) }
  get slug() { return this.__attr(`slug`) }
  get websiteUrl() { return this.__attr(`websiteUrl`) }
  get logoUrl() { return this.__attr(`logoUrl`) }
  get twitter() { return this.__attr(`twitter`) }
  get emailed() { return this.__attr(`emailed`) }
  get createdAt() { return this.__attr(`createdAt`) }
  get updatedAt() { return this.__attr(`updatedAt`) }
  jobs(builder?: string | JobModelSelector | ((selector: JobModelSelector) => JobModelSelector)) { return this.__child(`jobs`, JobModelSelector, builder) }
}
export function selectFromCompany() {
  return new CompanyModelSelector()
}

export const companyModelPrimitives = selectFromCompany().name.slug.websiteUrl.logoUrl.twitter.emailed.createdAt.updatedAt
