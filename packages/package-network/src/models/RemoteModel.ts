import { Instance } from "mobx-state-tree"
import { RemoteModelBase } from "./RemoteModel.base"

/* The TypeScript type of an instance of RemoteModel */
export interface RemoteModelType extends Instance<typeof RemoteModel.Type> {}

/* A graphql query fragment builders for RemoteModel */
export { selectFromRemote, remoteModelPrimitives, RemoteModelSelector } from "./RemoteModel.base"

/**
 * RemoteModel
 */
export const RemoteModel = RemoteModelBase
  .actions(self => ({
    // This is an auto-generated example action.
    log() {
      console.log(JSON.stringify(self))
    }
  }))
