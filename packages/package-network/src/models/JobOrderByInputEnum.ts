/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */
import { types } from "mobx-state-tree"

/**
 * Typescript enum
 */

export enum JobOrderByInput {
  id_ASC="id_ASC",
id_DESC="id_DESC",
title_ASC="title_ASC",
title_DESC="title_DESC",
slug_ASC="slug_ASC",
slug_DESC="slug_DESC",
description_ASC="description_ASC",
description_DESC="description_DESC",
applyUrl_ASC="applyUrl_ASC",
applyUrl_DESC="applyUrl_DESC",
isPublished_ASC="isPublished_ASC",
isPublished_DESC="isPublished_DESC",
isFeatured_ASC="isFeatured_ASC",
isFeatured_DESC="isFeatured_DESC",
locationNames_ASC="locationNames_ASC",
locationNames_DESC="locationNames_DESC",
userEmail_ASC="userEmail_ASC",
userEmail_DESC="userEmail_DESC",
postedAt_ASC="postedAt_ASC",
postedAt_DESC="postedAt_DESC",
createdAt_ASC="createdAt_ASC",
createdAt_DESC="createdAt_DESC",
updatedAt_ASC="updatedAt_ASC",
updatedAt_DESC="updatedAt_DESC"
}

/**
* JobOrderByInput
*/
export const JobOrderByInputEnumType = types.enumeration("JobOrderByInput", [
        "id_ASC",
  "id_DESC",
  "title_ASC",
  "title_DESC",
  "slug_ASC",
  "slug_DESC",
  "description_ASC",
  "description_DESC",
  "applyUrl_ASC",
  "applyUrl_DESC",
  "isPublished_ASC",
  "isPublished_DESC",
  "isFeatured_ASC",
  "isFeatured_DESC",
  "locationNames_ASC",
  "locationNames_DESC",
  "userEmail_ASC",
  "userEmail_DESC",
  "postedAt_ASC",
  "postedAt_DESC",
  "createdAt_ASC",
  "createdAt_DESC",
  "updatedAt_ASC",
  "updatedAt_DESC",
      ])
