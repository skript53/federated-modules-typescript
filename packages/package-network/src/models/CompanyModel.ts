import { Instance } from "mobx-state-tree"
import { CompanyModelBase } from "./CompanyModel.base"

/* The TypeScript type of an instance of CompanyModel */
export interface CompanyModelType extends Instance<typeof CompanyModel.Type> {}

/* A graphql query fragment builders for CompanyModel */
export { selectFromCompany, companyModelPrimitives, CompanyModelSelector } from "./CompanyModel.base"

/**
 * CompanyModel
 */
export const CompanyModel = CompanyModelBase
  .actions(self => ({
    // This is an auto-generated example action.
    log() {
      console.log(JSON.stringify(self))
    }
  }))
