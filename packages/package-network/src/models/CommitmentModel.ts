import { Instance } from "mobx-state-tree"
import { CommitmentModelBase } from "./CommitmentModel.base"

/* The TypeScript type of an instance of CommitmentModel */
export interface CommitmentModelType extends Instance<typeof CommitmentModel.Type> {}

/* A graphql query fragment builders for CommitmentModel */
export { selectFromCommitment, commitmentModelPrimitives, CommitmentModelSelector } from "./CommitmentModel.base"

/**
 * CommitmentModel
 */
export const CommitmentModel = CommitmentModelBase
  .actions(self => ({
    // This is an auto-generated example action.
    log() {
      console.log(JSON.stringify(self))
    }
  }))
