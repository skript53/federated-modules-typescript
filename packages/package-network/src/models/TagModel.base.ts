/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */

import { IObservableArray } from "mobx"
import { types } from "mobx-state-tree"
import { MSTGQLRef, QueryBuilder, withTypedRefs } from "mst-gql"
import { ModelBase } from "./ModelBase"
import { JobModel, JobModelType } from "./JobModel"
import { JobModelSelector } from "./JobModel.base"
import { RootStoreType } from "./index"


/* The TypeScript type that explicits the refs to other models in order to prevent a circular refs issue */
type Refs = {
  jobs: IObservableArray<JobModelType>;
}

/**
 * TagBase
 * auto generated base class for the model TagModel.
 */
export const TagModelBase = withTypedRefs<Refs>()(ModelBase
  .named('Tag')
  .props({
    __typename: types.optional(types.literal("Tag"), "Tag"),
    id: types.identifier,
    name: types.union(types.undefined, types.string),
    slug: types.union(types.undefined, types.string),
    jobs: types.union(types.undefined, types.null, types.array(MSTGQLRef(types.late((): any => JobModel)))),
    createdAt: types.union(types.undefined, types.frozen()),
    updatedAt: types.union(types.undefined, types.frozen()),
  })
  .views(self => ({
    get store() {
      return self.__getStore<RootStoreType>()
    }
  })))

export class TagModelSelector extends QueryBuilder {
  get id() { return this.__attr(`id`) }
  get name() { return this.__attr(`name`) }
  get slug() { return this.__attr(`slug`) }
  get createdAt() { return this.__attr(`createdAt`) }
  get updatedAt() { return this.__attr(`updatedAt`) }
  jobs(builder?: string | JobModelSelector | ((selector: JobModelSelector) => JobModelSelector)) { return this.__child(`jobs`, JobModelSelector, builder) }
}
export function selectFromTag() {
  return new TagModelSelector()
}

export const tagModelPrimitives = selectFromTag().name.slug.createdAt.updatedAt
