/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */

import { IObservableArray } from "mobx"
import { types } from "mobx-state-tree"
import { MSTGQLRef, QueryBuilder, withTypedRefs } from "mst-gql"
import { ModelBase } from "./ModelBase"
import { CityModel, CityModelType } from "./CityModel"
import { CityModelSelector } from "./CityModel.base"
import { JobModel, JobModelType } from "./JobModel"
import { JobModelSelector } from "./JobModel.base"
import { RootStoreType } from "./index"


/* The TypeScript type that explicits the refs to other models in order to prevent a circular refs issue */
type Refs = {
  cities: IObservableArray<CityModelType>;
  jobs: IObservableArray<JobModelType>;
}

/**
 * CountryBase
 * auto generated base class for the model CountryModel.
 */
export const CountryModelBase = withTypedRefs<Refs>()(ModelBase
  .named('Country')
  .props({
    __typename: types.optional(types.literal("Country"), "Country"),
    id: types.identifier,
    name: types.union(types.undefined, types.string),
    slug: types.union(types.undefined, types.string),
    type: types.union(types.undefined, types.string),
    isoCode: types.union(types.undefined, types.null, types.string),
    cities: types.union(types.undefined, types.null, types.array(MSTGQLRef(types.late((): any => CityModel)))),
    jobs: types.union(types.undefined, types.null, types.array(MSTGQLRef(types.late((): any => JobModel)))),
    createdAt: types.union(types.undefined, types.frozen()),
    updatedAt: types.union(types.undefined, types.frozen()),
  })
  .views(self => ({
    get store() {
      return self.__getStore<RootStoreType>()
    }
  })))

export class CountryModelSelector extends QueryBuilder {
  get id() { return this.__attr(`id`) }
  get name() { return this.__attr(`name`) }
  get slug() { return this.__attr(`slug`) }
  get type() { return this.__attr(`type`) }
  get isoCode() { return this.__attr(`isoCode`) }
  get createdAt() { return this.__attr(`createdAt`) }
  get updatedAt() { return this.__attr(`updatedAt`) }
  cities(builder?: string | CityModelSelector | ((selector: CityModelSelector) => CityModelSelector)) { return this.__child(`cities`, CityModelSelector, builder) }
  jobs(builder?: string | JobModelSelector | ((selector: JobModelSelector) => JobModelSelector)) { return this.__child(`jobs`, JobModelSelector, builder) }
}
export function selectFromCountry() {
  return new CountryModelSelector()
}

export const countryModelPrimitives = selectFromCountry().name.slug.type.isoCode.createdAt.updatedAt
