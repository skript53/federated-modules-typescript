/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */

import { IObservableArray } from "mobx"
import { types } from "mobx-state-tree"
import { MSTGQLRef, QueryBuilder, withTypedRefs } from "mst-gql"
import { ModelBase } from "./ModelBase"
import { CityModel, CityModelType } from "./CityModel"
import { CityModelSelector } from "./CityModel.base"
import { CommitmentModel, CommitmentModelType } from "./CommitmentModel"
import { CommitmentModelSelector } from "./CommitmentModel.base"
import { CompanyModel, CompanyModelType } from "./CompanyModel"
import { CompanyModelSelector } from "./CompanyModel.base"
import { CountryModel, CountryModelType } from "./CountryModel"
import { CountryModelSelector } from "./CountryModel.base"
import { RemoteModel, RemoteModelType } from "./RemoteModel"
import { RemoteModelSelector } from "./RemoteModel.base"
import { TagModel, TagModelType } from "./TagModel"
import { TagModelSelector } from "./TagModel.base"
import { RootStoreType } from "./index"


/* The TypeScript type that explicits the refs to other models in order to prevent a circular refs issue */
type Refs = {
  commitment: CommitmentModelType;
  cities: IObservableArray<CityModelType>;
  countries: IObservableArray<CountryModelType>;
  remotes: IObservableArray<RemoteModelType>;
  company: CompanyModelType;
  tags: IObservableArray<TagModelType>;
}

/**
 * JobBase
 * auto generated base class for the model JobModel.
 */
export const JobModelBase = withTypedRefs<Refs>()(ModelBase
  .named('Job')
  .props({
    __typename: types.optional(types.literal("Job"), "Job"),
    id: types.identifier,
    title: types.union(types.undefined, types.string),
    slug: types.union(types.undefined, types.string),
    commitment: types.union(types.undefined, MSTGQLRef(types.late((): any => CommitmentModel))),
    cities: types.union(types.undefined, types.null, types.array(MSTGQLRef(types.late((): any => CityModel)))),
    countries: types.union(types.undefined, types.null, types.array(MSTGQLRef(types.late((): any => CountryModel)))),
    remotes: types.union(types.undefined, types.null, types.array(MSTGQLRef(types.late((): any => RemoteModel)))),
    description: types.union(types.undefined, types.null, types.string),
    applyUrl: types.union(types.undefined, types.null, types.string),
    company: types.union(types.undefined, types.null, MSTGQLRef(types.late((): any => CompanyModel))),
    tags: types.union(types.undefined, types.null, types.array(MSTGQLRef(types.late((): any => TagModel)))),
    isPublished: types.union(types.undefined, types.null, types.boolean),
    isFeatured: types.union(types.undefined, types.null, types.boolean),
    locationNames: types.union(types.undefined, types.null, types.string),
    userEmail: types.union(types.undefined, types.null, types.string),
    postedAt: types.union(types.undefined, types.frozen()),
    createdAt: types.union(types.undefined, types.frozen()),
    updatedAt: types.union(types.undefined, types.frozen()),
  })
  .views(self => ({
    get store() {
      return self.__getStore<RootStoreType>()
    }
  })))

export class JobModelSelector extends QueryBuilder {
  get id() { return this.__attr(`id`) }
  get title() { return this.__attr(`title`) }
  get slug() { return this.__attr(`slug`) }
  get description() { return this.__attr(`description`) }
  get applyUrl() { return this.__attr(`applyUrl`) }
  get isPublished() { return this.__attr(`isPublished`) }
  get isFeatured() { return this.__attr(`isFeatured`) }
  get locationNames() { return this.__attr(`locationNames`) }
  get userEmail() { return this.__attr(`userEmail`) }
  get postedAt() { return this.__attr(`postedAt`) }
  get createdAt() { return this.__attr(`createdAt`) }
  get updatedAt() { return this.__attr(`updatedAt`) }
  commitment(builder?: string | CommitmentModelSelector | ((selector: CommitmentModelSelector) => CommitmentModelSelector)) { return this.__child(`commitment`, CommitmentModelSelector, builder) }
  cities(builder?: string | CityModelSelector | ((selector: CityModelSelector) => CityModelSelector)) { return this.__child(`cities`, CityModelSelector, builder) }
  countries(builder?: string | CountryModelSelector | ((selector: CountryModelSelector) => CountryModelSelector)) { return this.__child(`countries`, CountryModelSelector, builder) }
  remotes(builder?: string | RemoteModelSelector | ((selector: RemoteModelSelector) => RemoteModelSelector)) { return this.__child(`remotes`, RemoteModelSelector, builder) }
  company(builder?: string | CompanyModelSelector | ((selector: CompanyModelSelector) => CompanyModelSelector)) { return this.__child(`company`, CompanyModelSelector, builder) }
  tags(builder?: string | TagModelSelector | ((selector: TagModelSelector) => TagModelSelector)) { return this.__child(`tags`, TagModelSelector, builder) }
}
export function selectFromJob() {
  return new JobModelSelector()
}

export const jobModelPrimitives = selectFromJob().title.slug.description.applyUrl.isPublished.isFeatured.locationNames.userEmail.postedAt.createdAt.updatedAt
