/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */
import { types } from "mobx-state-tree"

/**
 * Typescript enum
 */

export enum TagOrderByInput {
  id_ASC="id_ASC",
id_DESC="id_DESC",
name_ASC="name_ASC",
name_DESC="name_DESC",
slug_ASC="slug_ASC",
slug_DESC="slug_DESC",
createdAt_ASC="createdAt_ASC",
createdAt_DESC="createdAt_DESC",
updatedAt_ASC="updatedAt_ASC",
updatedAt_DESC="updatedAt_DESC"
}

/**
* TagOrderByInput
*/
export const TagOrderByInputEnumType = types.enumeration("TagOrderByInput", [
        "id_ASC",
  "id_DESC",
  "name_ASC",
  "name_DESC",
  "slug_ASC",
  "slug_DESC",
  "createdAt_ASC",
  "createdAt_DESC",
  "updatedAt_ASC",
  "updatedAt_DESC",
      ])
