import { Instance } from "mobx-state-tree"
import { JobModelBase } from "./JobModel.base"

/* The TypeScript type of an instance of JobModel */
export interface JobModelType extends Instance<typeof JobModel.Type> {}

/* A graphql query fragment builders for JobModel */
export { selectFromJob, jobModelPrimitives, JobModelSelector } from "./JobModel.base"

/**
 * JobModel
 */
export const JobModel = JobModelBase
  .actions(self => ({
    // This is an auto-generated example action.
    log() {
      console.log(JSON.stringify(self))
    }
  }))
