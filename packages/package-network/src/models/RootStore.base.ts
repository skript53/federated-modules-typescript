/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */
import { ObservableMap } from "mobx"
import { types } from "mobx-state-tree"
import { MSTGQLStore, configureStoreMixin, QueryOptions, withTypedRefs } from "mst-gql"

import { JobModel, JobModelType } from "./JobModel"
import { jobModelPrimitives, JobModelSelector } from "./JobModel.base"
import { CommitmentModel, CommitmentModelType } from "./CommitmentModel"
import { commitmentModelPrimitives, CommitmentModelSelector } from "./CommitmentModel.base"
import { CityModel, CityModelType } from "./CityModel"
import { cityModelPrimitives, CityModelSelector } from "./CityModel.base"
import { CountryModel, CountryModelType } from "./CountryModel"
import { countryModelPrimitives, CountryModelSelector } from "./CountryModel.base"
import { RemoteModel, RemoteModelType } from "./RemoteModel"
import { remoteModelPrimitives, RemoteModelSelector } from "./RemoteModel.base"
import { CompanyModel, CompanyModelType } from "./CompanyModel"
import { companyModelPrimitives, CompanyModelSelector } from "./CompanyModel.base"
import { TagModel, TagModelType } from "./TagModel"
import { tagModelPrimitives, TagModelSelector } from "./TagModel.base"
import { LocationModel, LocationModelType } from "./LocationModel"
import { locationModelPrimitives, LocationModelSelector } from "./LocationModel.base"
import { UserModel, UserModelType } from "./UserModel"
import { userModelPrimitives, UserModelSelector } from "./UserModel.base"


import { JobOrderByInput } from "./JobOrderByInputEnum"
import { CityOrderByInput } from "./CityOrderByInputEnum"
import { CountryOrderByInput } from "./CountryOrderByInputEnum"
import { RemoteOrderByInput } from "./RemoteOrderByInputEnum"
import { TagOrderByInput } from "./TagOrderByInputEnum"

export type JobsInput = {
  type?: string
  slug?: string
}
export type JobWhereInput = {
  id?: string
  id_not?: string
  id_in?: string[]
  id_not_in?: string[]
  id_lt?: string
  id_lte?: string
  id_gt?: string
  id_gte?: string
  id_contains?: string
  id_not_contains?: string
  id_starts_with?: string
  id_not_starts_with?: string
  id_ends_with?: string
  id_not_ends_with?: string
  title?: string
  title_not?: string
  title_in?: string[]
  title_not_in?: string[]
  title_lt?: string
  title_lte?: string
  title_gt?: string
  title_gte?: string
  title_contains?: string
  title_not_contains?: string
  title_starts_with?: string
  title_not_starts_with?: string
  title_ends_with?: string
  title_not_ends_with?: string
  slug?: string
  slug_not?: string
  slug_in?: string[]
  slug_not_in?: string[]
  slug_lt?: string
  slug_lte?: string
  slug_gt?: string
  slug_gte?: string
  slug_contains?: string
  slug_not_contains?: string
  slug_starts_with?: string
  slug_not_starts_with?: string
  slug_ends_with?: string
  slug_not_ends_with?: string
  commitment?: CommitmentWhereInput
  cities_every?: CityWhereInput
  cities_some?: CityWhereInput
  cities_none?: CityWhereInput
  countries_every?: CountryWhereInput
  countries_some?: CountryWhereInput
  countries_none?: CountryWhereInput
  remotes_every?: RemoteWhereInput
  remotes_some?: RemoteWhereInput
  remotes_none?: RemoteWhereInput
  description?: string
  description_not?: string
  description_in?: string[]
  description_not_in?: string[]
  description_lt?: string
  description_lte?: string
  description_gt?: string
  description_gte?: string
  description_contains?: string
  description_not_contains?: string
  description_starts_with?: string
  description_not_starts_with?: string
  description_ends_with?: string
  description_not_ends_with?: string
  applyUrl?: string
  applyUrl_not?: string
  applyUrl_in?: string[]
  applyUrl_not_in?: string[]
  applyUrl_lt?: string
  applyUrl_lte?: string
  applyUrl_gt?: string
  applyUrl_gte?: string
  applyUrl_contains?: string
  applyUrl_not_contains?: string
  applyUrl_starts_with?: string
  applyUrl_not_starts_with?: string
  applyUrl_ends_with?: string
  applyUrl_not_ends_with?: string
  company?: CompanyWhereInput
  tags_every?: TagWhereInput
  tags_some?: TagWhereInput
  tags_none?: TagWhereInput
  isPublished?: boolean
  isPublished_not?: boolean
  isFeatured?: boolean
  isFeatured_not?: boolean
  locationNames?: string
  locationNames_not?: string
  locationNames_in?: string[]
  locationNames_not_in?: string[]
  locationNames_lt?: string
  locationNames_lte?: string
  locationNames_gt?: string
  locationNames_gte?: string
  locationNames_contains?: string
  locationNames_not_contains?: string
  locationNames_starts_with?: string
  locationNames_not_starts_with?: string
  locationNames_ends_with?: string
  locationNames_not_ends_with?: string
  userEmail?: string
  userEmail_not?: string
  userEmail_in?: string[]
  userEmail_not_in?: string[]
  userEmail_lt?: string
  userEmail_lte?: string
  userEmail_gt?: string
  userEmail_gte?: string
  userEmail_contains?: string
  userEmail_not_contains?: string
  userEmail_starts_with?: string
  userEmail_not_starts_with?: string
  userEmail_ends_with?: string
  userEmail_not_ends_with?: string
  postedAt?: any
  postedAt_not?: any
  postedAt_in?: any[]
  postedAt_not_in?: any[]
  postedAt_lt?: any
  postedAt_lte?: any
  postedAt_gt?: any
  postedAt_gte?: any
  createdAt?: any
  createdAt_not?: any
  createdAt_in?: any[]
  createdAt_not_in?: any[]
  createdAt_lt?: any
  createdAt_lte?: any
  createdAt_gt?: any
  createdAt_gte?: any
  updatedAt?: any
  updatedAt_not?: any
  updatedAt_in?: any[]
  updatedAt_not_in?: any[]
  updatedAt_lt?: any
  updatedAt_lte?: any
  updatedAt_gt?: any
  updatedAt_gte?: any
  AND?: JobWhereInput[]
  OR?: JobWhereInput[]
  NOT?: JobWhereInput[]
}
export type CommitmentWhereInput = {
  id?: string
  id_not?: string
  id_in?: string[]
  id_not_in?: string[]
  id_lt?: string
  id_lte?: string
  id_gt?: string
  id_gte?: string
  id_contains?: string
  id_not_contains?: string
  id_starts_with?: string
  id_not_starts_with?: string
  id_ends_with?: string
  id_not_ends_with?: string
  title?: string
  title_not?: string
  title_in?: string[]
  title_not_in?: string[]
  title_lt?: string
  title_lte?: string
  title_gt?: string
  title_gte?: string
  title_contains?: string
  title_not_contains?: string
  title_starts_with?: string
  title_not_starts_with?: string
  title_ends_with?: string
  title_not_ends_with?: string
  slug?: string
  slug_not?: string
  slug_in?: string[]
  slug_not_in?: string[]
  slug_lt?: string
  slug_lte?: string
  slug_gt?: string
  slug_gte?: string
  slug_contains?: string
  slug_not_contains?: string
  slug_starts_with?: string
  slug_not_starts_with?: string
  slug_ends_with?: string
  slug_not_ends_with?: string
  jobs_every?: JobWhereInput
  jobs_some?: JobWhereInput
  jobs_none?: JobWhereInput
  createdAt?: any
  createdAt_not?: any
  createdAt_in?: any[]
  createdAt_not_in?: any[]
  createdAt_lt?: any
  createdAt_lte?: any
  createdAt_gt?: any
  createdAt_gte?: any
  updatedAt?: any
  updatedAt_not?: any
  updatedAt_in?: any[]
  updatedAt_not_in?: any[]
  updatedAt_lt?: any
  updatedAt_lte?: any
  updatedAt_gt?: any
  updatedAt_gte?: any
  AND?: CommitmentWhereInput[]
  OR?: CommitmentWhereInput[]
  NOT?: CommitmentWhereInput[]
}
export type CityWhereInput = {
  id?: string
  id_not?: string
  id_in?: string[]
  id_not_in?: string[]
  id_lt?: string
  id_lte?: string
  id_gt?: string
  id_gte?: string
  id_contains?: string
  id_not_contains?: string
  id_starts_with?: string
  id_not_starts_with?: string
  id_ends_with?: string
  id_not_ends_with?: string
  name?: string
  name_not?: string
  name_in?: string[]
  name_not_in?: string[]
  name_lt?: string
  name_lte?: string
  name_gt?: string
  name_gte?: string
  name_contains?: string
  name_not_contains?: string
  name_starts_with?: string
  name_not_starts_with?: string
  name_ends_with?: string
  name_not_ends_with?: string
  slug?: string
  slug_not?: string
  slug_in?: string[]
  slug_not_in?: string[]
  slug_lt?: string
  slug_lte?: string
  slug_gt?: string
  slug_gte?: string
  slug_contains?: string
  slug_not_contains?: string
  slug_starts_with?: string
  slug_not_starts_with?: string
  slug_ends_with?: string
  slug_not_ends_with?: string
  country?: CountryWhereInput
  type?: string
  type_not?: string
  type_in?: string[]
  type_not_in?: string[]
  type_lt?: string
  type_lte?: string
  type_gt?: string
  type_gte?: string
  type_contains?: string
  type_not_contains?: string
  type_starts_with?: string
  type_not_starts_with?: string
  type_ends_with?: string
  type_not_ends_with?: string
  jobs_every?: JobWhereInput
  jobs_some?: JobWhereInput
  jobs_none?: JobWhereInput
  createdAt?: any
  createdAt_not?: any
  createdAt_in?: any[]
  createdAt_not_in?: any[]
  createdAt_lt?: any
  createdAt_lte?: any
  createdAt_gt?: any
  createdAt_gte?: any
  updatedAt?: any
  updatedAt_not?: any
  updatedAt_in?: any[]
  updatedAt_not_in?: any[]
  updatedAt_lt?: any
  updatedAt_lte?: any
  updatedAt_gt?: any
  updatedAt_gte?: any
  AND?: CityWhereInput[]
  OR?: CityWhereInput[]
  NOT?: CityWhereInput[]
}
export type CountryWhereInput = {
  id?: string
  id_not?: string
  id_in?: string[]
  id_not_in?: string[]
  id_lt?: string
  id_lte?: string
  id_gt?: string
  id_gte?: string
  id_contains?: string
  id_not_contains?: string
  id_starts_with?: string
  id_not_starts_with?: string
  id_ends_with?: string
  id_not_ends_with?: string
  name?: string
  name_not?: string
  name_in?: string[]
  name_not_in?: string[]
  name_lt?: string
  name_lte?: string
  name_gt?: string
  name_gte?: string
  name_contains?: string
  name_not_contains?: string
  name_starts_with?: string
  name_not_starts_with?: string
  name_ends_with?: string
  name_not_ends_with?: string
  slug?: string
  slug_not?: string
  slug_in?: string[]
  slug_not_in?: string[]
  slug_lt?: string
  slug_lte?: string
  slug_gt?: string
  slug_gte?: string
  slug_contains?: string
  slug_not_contains?: string
  slug_starts_with?: string
  slug_not_starts_with?: string
  slug_ends_with?: string
  slug_not_ends_with?: string
  type?: string
  type_not?: string
  type_in?: string[]
  type_not_in?: string[]
  type_lt?: string
  type_lte?: string
  type_gt?: string
  type_gte?: string
  type_contains?: string
  type_not_contains?: string
  type_starts_with?: string
  type_not_starts_with?: string
  type_ends_with?: string
  type_not_ends_with?: string
  isoCode?: string
  isoCode_not?: string
  isoCode_in?: string[]
  isoCode_not_in?: string[]
  isoCode_lt?: string
  isoCode_lte?: string
  isoCode_gt?: string
  isoCode_gte?: string
  isoCode_contains?: string
  isoCode_not_contains?: string
  isoCode_starts_with?: string
  isoCode_not_starts_with?: string
  isoCode_ends_with?: string
  isoCode_not_ends_with?: string
  cities_every?: CityWhereInput
  cities_some?: CityWhereInput
  cities_none?: CityWhereInput
  jobs_every?: JobWhereInput
  jobs_some?: JobWhereInput
  jobs_none?: JobWhereInput
  createdAt?: any
  createdAt_not?: any
  createdAt_in?: any[]
  createdAt_not_in?: any[]
  createdAt_lt?: any
  createdAt_lte?: any
  createdAt_gt?: any
  createdAt_gte?: any
  updatedAt?: any
  updatedAt_not?: any
  updatedAt_in?: any[]
  updatedAt_not_in?: any[]
  updatedAt_lt?: any
  updatedAt_lte?: any
  updatedAt_gt?: any
  updatedAt_gte?: any
  AND?: CountryWhereInput[]
  OR?: CountryWhereInput[]
  NOT?: CountryWhereInput[]
}
export type RemoteWhereInput = {
  id?: string
  id_not?: string
  id_in?: string[]
  id_not_in?: string[]
  id_lt?: string
  id_lte?: string
  id_gt?: string
  id_gte?: string
  id_contains?: string
  id_not_contains?: string
  id_starts_with?: string
  id_not_starts_with?: string
  id_ends_with?: string
  id_not_ends_with?: string
  name?: string
  name_not?: string
  name_in?: string[]
  name_not_in?: string[]
  name_lt?: string
  name_lte?: string
  name_gt?: string
  name_gte?: string
  name_contains?: string
  name_not_contains?: string
  name_starts_with?: string
  name_not_starts_with?: string
  name_ends_with?: string
  name_not_ends_with?: string
  slug?: string
  slug_not?: string
  slug_in?: string[]
  slug_not_in?: string[]
  slug_lt?: string
  slug_lte?: string
  slug_gt?: string
  slug_gte?: string
  slug_contains?: string
  slug_not_contains?: string
  slug_starts_with?: string
  slug_not_starts_with?: string
  slug_ends_with?: string
  slug_not_ends_with?: string
  type?: string
  type_not?: string
  type_in?: string[]
  type_not_in?: string[]
  type_lt?: string
  type_lte?: string
  type_gt?: string
  type_gte?: string
  type_contains?: string
  type_not_contains?: string
  type_starts_with?: string
  type_not_starts_with?: string
  type_ends_with?: string
  type_not_ends_with?: string
  jobs_every?: JobWhereInput
  jobs_some?: JobWhereInput
  jobs_none?: JobWhereInput
  createdAt?: any
  createdAt_not?: any
  createdAt_in?: any[]
  createdAt_not_in?: any[]
  createdAt_lt?: any
  createdAt_lte?: any
  createdAt_gt?: any
  createdAt_gte?: any
  updatedAt?: any
  updatedAt_not?: any
  updatedAt_in?: any[]
  updatedAt_not_in?: any[]
  updatedAt_lt?: any
  updatedAt_lte?: any
  updatedAt_gt?: any
  updatedAt_gte?: any
  AND?: RemoteWhereInput[]
  OR?: RemoteWhereInput[]
  NOT?: RemoteWhereInput[]
}
export type CompanyWhereInput = {
  id?: string
  id_not?: string
  id_in?: string[]
  id_not_in?: string[]
  id_lt?: string
  id_lte?: string
  id_gt?: string
  id_gte?: string
  id_contains?: string
  id_not_contains?: string
  id_starts_with?: string
  id_not_starts_with?: string
  id_ends_with?: string
  id_not_ends_with?: string
  name?: string
  name_not?: string
  name_in?: string[]
  name_not_in?: string[]
  name_lt?: string
  name_lte?: string
  name_gt?: string
  name_gte?: string
  name_contains?: string
  name_not_contains?: string
  name_starts_with?: string
  name_not_starts_with?: string
  name_ends_with?: string
  name_not_ends_with?: string
  slug?: string
  slug_not?: string
  slug_in?: string[]
  slug_not_in?: string[]
  slug_lt?: string
  slug_lte?: string
  slug_gt?: string
  slug_gte?: string
  slug_contains?: string
  slug_not_contains?: string
  slug_starts_with?: string
  slug_not_starts_with?: string
  slug_ends_with?: string
  slug_not_ends_with?: string
  websiteUrl?: string
  websiteUrl_not?: string
  websiteUrl_in?: string[]
  websiteUrl_not_in?: string[]
  websiteUrl_lt?: string
  websiteUrl_lte?: string
  websiteUrl_gt?: string
  websiteUrl_gte?: string
  websiteUrl_contains?: string
  websiteUrl_not_contains?: string
  websiteUrl_starts_with?: string
  websiteUrl_not_starts_with?: string
  websiteUrl_ends_with?: string
  websiteUrl_not_ends_with?: string
  logoUrl?: string
  logoUrl_not?: string
  logoUrl_in?: string[]
  logoUrl_not_in?: string[]
  logoUrl_lt?: string
  logoUrl_lte?: string
  logoUrl_gt?: string
  logoUrl_gte?: string
  logoUrl_contains?: string
  logoUrl_not_contains?: string
  logoUrl_starts_with?: string
  logoUrl_not_starts_with?: string
  logoUrl_ends_with?: string
  logoUrl_not_ends_with?: string
  jobs_every?: JobWhereInput
  jobs_some?: JobWhereInput
  jobs_none?: JobWhereInput
  twitter?: string
  twitter_not?: string
  twitter_in?: string[]
  twitter_not_in?: string[]
  twitter_lt?: string
  twitter_lte?: string
  twitter_gt?: string
  twitter_gte?: string
  twitter_contains?: string
  twitter_not_contains?: string
  twitter_starts_with?: string
  twitter_not_starts_with?: string
  twitter_ends_with?: string
  twitter_not_ends_with?: string
  emailed?: boolean
  emailed_not?: boolean
  createdAt?: any
  createdAt_not?: any
  createdAt_in?: any[]
  createdAt_not_in?: any[]
  createdAt_lt?: any
  createdAt_lte?: any
  createdAt_gt?: any
  createdAt_gte?: any
  updatedAt?: any
  updatedAt_not?: any
  updatedAt_in?: any[]
  updatedAt_not_in?: any[]
  updatedAt_lt?: any
  updatedAt_lte?: any
  updatedAt_gt?: any
  updatedAt_gte?: any
  AND?: CompanyWhereInput[]
  OR?: CompanyWhereInput[]
  NOT?: CompanyWhereInput[]
}
export type TagWhereInput = {
  id?: string
  id_not?: string
  id_in?: string[]
  id_not_in?: string[]
  id_lt?: string
  id_lte?: string
  id_gt?: string
  id_gte?: string
  id_contains?: string
  id_not_contains?: string
  id_starts_with?: string
  id_not_starts_with?: string
  id_ends_with?: string
  id_not_ends_with?: string
  name?: string
  name_not?: string
  name_in?: string[]
  name_not_in?: string[]
  name_lt?: string
  name_lte?: string
  name_gt?: string
  name_gte?: string
  name_contains?: string
  name_not_contains?: string
  name_starts_with?: string
  name_not_starts_with?: string
  name_ends_with?: string
  name_not_ends_with?: string
  slug?: string
  slug_not?: string
  slug_in?: string[]
  slug_not_in?: string[]
  slug_lt?: string
  slug_lte?: string
  slug_gt?: string
  slug_gte?: string
  slug_contains?: string
  slug_not_contains?: string
  slug_starts_with?: string
  slug_not_starts_with?: string
  slug_ends_with?: string
  slug_not_ends_with?: string
  jobs_every?: JobWhereInput
  jobs_some?: JobWhereInput
  jobs_none?: JobWhereInput
  createdAt?: any
  createdAt_not?: any
  createdAt_in?: any[]
  createdAt_not_in?: any[]
  createdAt_lt?: any
  createdAt_lte?: any
  createdAt_gt?: any
  createdAt_gte?: any
  updatedAt?: any
  updatedAt_not?: any
  updatedAt_in?: any[]
  updatedAt_not_in?: any[]
  updatedAt_lt?: any
  updatedAt_lte?: any
  updatedAt_gt?: any
  updatedAt_gte?: any
  AND?: TagWhereInput[]
  OR?: TagWhereInput[]
  NOT?: TagWhereInput[]
}
export type JobInput = {
  companySlug: string
  jobSlug: string
}
export type LocationsInput = {
  value: string
}
export type LocationInput = {
  slug: string
}
export type SubscribeInput = {
  name: string
  email: string
}
export type PostJobInput = {
  title: string
  commitmentId: string
  companyName: string
  locationNames: string
  userEmail: string
  description: string
  applyUrl: string
}
export type UpdateJobInput = {
  id: string
  description: string
}
export type UpdateCompanyInput = {
  id: string
  logoUrl: string
}
/* The TypeScript type that explicits the refs to other models in order to prevent a circular refs issue */
type Refs = {
  jobs: ObservableMap<string, JobModelType>,
  commitments: ObservableMap<string, CommitmentModelType>,
  cities: ObservableMap<string, CityModelType>,
  countries: ObservableMap<string, CountryModelType>,
  remotes: ObservableMap<string, RemoteModelType>,
  companies: ObservableMap<string, CompanyModelType>,
  tags: ObservableMap<string, TagModelType>,
  locations: ObservableMap<string, LocationModelType>,
  users: ObservableMap<string, UserModelType>
}


/**
* Enums for the names of base graphql actions
*/
export enum RootStoreBaseQueries {
queryJobs="queryJobs",
queryJob="queryJob",
queryLocations="queryLocations",
queryCity="queryCity",
queryCountry="queryCountry",
queryRemote="queryRemote",
queryCommitments="queryCommitments",
queryCities="queryCities",
queryCountries="queryCountries",
queryRemotes="queryRemotes",
queryCompanies="queryCompanies"
}
export enum RootStoreBaseMutations {
mutateSubscribe="mutateSubscribe",
mutatePostJob="mutatePostJob",
mutateUpdateJob="mutateUpdateJob",
mutateUpdateCompany="mutateUpdateCompany"
}

/**
* Store, managing, among others, all the objects received through graphQL
*/
export const RootStoreBase = withTypedRefs<Refs>()(MSTGQLStore
  .named("RootStore")
  .extend(configureStoreMixin([['Job', () => JobModel], ['Commitment', () => CommitmentModel], ['City', () => CityModel], ['Country', () => CountryModel], ['Remote', () => RemoteModel], ['Company', () => CompanyModel], ['Tag', () => TagModel], ['Location', () => LocationModel], ['User', () => UserModel]], ['Job', 'Commitment', 'City', 'Country', 'Remote', 'Company', 'Tag', 'Location', 'User'], "js"))
  .props({
    jobs: types.optional(types.map(types.late((): any => JobModel)), {}),
    commitments: types.optional(types.map(types.late((): any => CommitmentModel)), {}),
    cities: types.optional(types.map(types.late((): any => CityModel)), {}),
    countries: types.optional(types.map(types.late((): any => CountryModel)), {}),
    remotes: types.optional(types.map(types.late((): any => RemoteModel)), {}),
    companies: types.optional(types.map(types.late((): any => CompanyModel)), {}),
    tags: types.optional(types.map(types.late((): any => TagModel)), {}),
    locations: types.optional(types.map(types.late((): any => LocationModel)), {}),
    users: types.optional(types.map(types.late((): any => UserModel)), {})
  })
  .actions(self => ({
    queryJobs(variables: { input?: JobsInput }, resultSelector: string | ((qb: JobModelSelector) => JobModelSelector) = jobModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ jobs: JobModelType[]}>(`query jobs($input: JobsInput) { jobs(input: $input) {
        ${typeof resultSelector === "function" ? resultSelector(new JobModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryJob(variables: { input: JobInput }, resultSelector: string | ((qb: JobModelSelector) => JobModelSelector) = jobModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ job: JobModelType}>(`query job($input: JobInput!) { job(input: $input) {
        ${typeof resultSelector === "function" ? resultSelector(new JobModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryLocations(variables: { input: LocationsInput }, resultSelector: string | ((qb: LocationModelSelector) => LocationModelSelector) = locationModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ locations: LocationModelType[]}>(`query locations($input: LocationsInput!) { locations(input: $input) {
        ${typeof resultSelector === "function" ? resultSelector(new LocationModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryCity(variables: { input: LocationInput }, resultSelector: string | ((qb: CityModelSelector) => CityModelSelector) = cityModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ city: CityModelType}>(`query city($input: LocationInput!) { city(input: $input) {
        ${typeof resultSelector === "function" ? resultSelector(new CityModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryCountry(variables: { input: LocationInput }, resultSelector: string | ((qb: CountryModelSelector) => CountryModelSelector) = countryModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ country: CountryModelType}>(`query country($input: LocationInput!) { country(input: $input) {
        ${typeof resultSelector === "function" ? resultSelector(new CountryModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryRemote(variables: { input: LocationInput }, resultSelector: string | ((qb: RemoteModelSelector) => RemoteModelSelector) = remoteModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ remote: RemoteModelType}>(`query remote($input: LocationInput!) { remote(input: $input) {
        ${typeof resultSelector === "function" ? resultSelector(new RemoteModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryCommitments(variables?: {  }, resultSelector: string | ((qb: CommitmentModelSelector) => CommitmentModelSelector) = commitmentModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ commitments: CommitmentModelType[]}>(`query commitments { commitments {
        ${typeof resultSelector === "function" ? resultSelector(new CommitmentModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryCities(variables?: {  }, resultSelector: string | ((qb: CityModelSelector) => CityModelSelector) = cityModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ cities: CityModelType[]}>(`query cities { cities {
        ${typeof resultSelector === "function" ? resultSelector(new CityModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryCountries(variables?: {  }, resultSelector: string | ((qb: CountryModelSelector) => CountryModelSelector) = countryModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ countries: CountryModelType[]}>(`query countries { countries {
        ${typeof resultSelector === "function" ? resultSelector(new CountryModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryRemotes(variables?: {  }, resultSelector: string | ((qb: RemoteModelSelector) => RemoteModelSelector) = remoteModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ remotes: RemoteModelType[]}>(`query remotes { remotes {
        ${typeof resultSelector === "function" ? resultSelector(new RemoteModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryCompanies(variables?: {  }, resultSelector: string | ((qb: CompanyModelSelector) => CompanyModelSelector) = companyModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ companies: CompanyModelType[]}>(`query companies { companies {
        ${typeof resultSelector === "function" ? resultSelector(new CompanyModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    mutateSubscribe(variables: { input: SubscribeInput }, resultSelector: string | ((qb: UserModelSelector) => UserModelSelector) = userModelPrimitives.toString(), optimisticUpdate?: () => void) {
      return self.mutate<{ subscribe: UserModelType}>(`mutation subscribe($input: SubscribeInput!) { subscribe(input: $input) {
        ${typeof resultSelector === "function" ? resultSelector(new UserModelSelector()).toString() : resultSelector}
      } }`, variables, optimisticUpdate)
    },
    mutatePostJob(variables: { input: PostJobInput }, resultSelector: string | ((qb: JobModelSelector) => JobModelSelector) = jobModelPrimitives.toString(), optimisticUpdate?: () => void) {
      return self.mutate<{ postJob: JobModelType}>(`mutation postJob($input: PostJobInput!) { postJob(input: $input) {
        ${typeof resultSelector === "function" ? resultSelector(new JobModelSelector()).toString() : resultSelector}
      } }`, variables, optimisticUpdate)
    },
    mutateUpdateJob(variables: { input: UpdateJobInput, adminSecret: string }, resultSelector: string | ((qb: JobModelSelector) => JobModelSelector) = jobModelPrimitives.toString(), optimisticUpdate?: () => void) {
      return self.mutate<{ updateJob: JobModelType}>(`mutation updateJob($input: UpdateJobInput!, $adminSecret: String!) { updateJob(input: $input, adminSecret: $adminSecret) {
        ${typeof resultSelector === "function" ? resultSelector(new JobModelSelector()).toString() : resultSelector}
      } }`, variables, optimisticUpdate)
    },
    mutateUpdateCompany(variables: { input: UpdateCompanyInput, adminSecret: string }, resultSelector: string | ((qb: CompanyModelSelector) => CompanyModelSelector) = companyModelPrimitives.toString(), optimisticUpdate?: () => void) {
      return self.mutate<{ updateCompany: CompanyModelType}>(`mutation updateCompany($input: UpdateCompanyInput!, $adminSecret: String!) { updateCompany(input: $input, adminSecret: $adminSecret) {
        ${typeof resultSelector === "function" ? resultSelector(new CompanyModelSelector()).toString() : resultSelector}
      } }`, variables, optimisticUpdate)
    },
  })))
