/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */
import { types } from "mobx-state-tree"

/**
 * Typescript enum
 */

export enum CityOrderByInput {
  id_ASC="id_ASC",
id_DESC="id_DESC",
name_ASC="name_ASC",
name_DESC="name_DESC",
slug_ASC="slug_ASC",
slug_DESC="slug_DESC",
type_ASC="type_ASC",
type_DESC="type_DESC",
createdAt_ASC="createdAt_ASC",
createdAt_DESC="createdAt_DESC",
updatedAt_ASC="updatedAt_ASC",
updatedAt_DESC="updatedAt_DESC"
}

/**
* CityOrderByInput
*/
export const CityOrderByInputEnumType = types.enumeration("CityOrderByInput", [
        "id_ASC",
  "id_DESC",
  "name_ASC",
  "name_DESC",
  "slug_ASC",
  "slug_DESC",
  "type_ASC",
  "type_DESC",
  "createdAt_ASC",
  "createdAt_DESC",
  "updatedAt_ASC",
  "updatedAt_DESC",
      ])
