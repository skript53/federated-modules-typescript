/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */

import { IObservableArray } from "mobx"
import { types } from "mobx-state-tree"
import { MSTGQLRef, QueryBuilder, withTypedRefs } from "mst-gql"
import { ModelBase } from "./ModelBase"
import { CountryModel, CountryModelType } from "./CountryModel"
import { CountryModelSelector } from "./CountryModel.base"
import { JobModel, JobModelType } from "./JobModel"
import { JobModelSelector } from "./JobModel.base"
import { RootStoreType } from "./index"


/* The TypeScript type that explicits the refs to other models in order to prevent a circular refs issue */
type Refs = {
  country: CountryModelType;
  jobs: IObservableArray<JobModelType>;
}

/**
 * CityBase
 * auto generated base class for the model CityModel.
 */
export const CityModelBase = withTypedRefs<Refs>()(ModelBase
  .named('City')
  .props({
    __typename: types.optional(types.literal("City"), "City"),
    id: types.identifier,
    name: types.union(types.undefined, types.string),
    slug: types.union(types.undefined, types.string),
    country: types.union(types.undefined, MSTGQLRef(types.late((): any => CountryModel))),
    type: types.union(types.undefined, types.string),
    jobs: types.union(types.undefined, types.null, types.array(MSTGQLRef(types.late((): any => JobModel)))),
    createdAt: types.union(types.undefined, types.frozen()),
    updatedAt: types.union(types.undefined, types.frozen()),
  })
  .views(self => ({
    get store() {
      return self.__getStore<RootStoreType>()
    }
  })))

export class CityModelSelector extends QueryBuilder {
  get id() { return this.__attr(`id`) }
  get name() { return this.__attr(`name`) }
  get slug() { return this.__attr(`slug`) }
  get type() { return this.__attr(`type`) }
  get createdAt() { return this.__attr(`createdAt`) }
  get updatedAt() { return this.__attr(`updatedAt`) }
  country(builder?: string | CountryModelSelector | ((selector: CountryModelSelector) => CountryModelSelector)) { return this.__child(`country`, CountryModelSelector, builder) }
  jobs(builder?: string | JobModelSelector | ((selector: JobModelSelector) => JobModelSelector)) { return this.__child(`jobs`, JobModelSelector, builder) }
}
export function selectFromCity() {
  return new CityModelSelector()
}

export const cityModelPrimitives = selectFromCity().name.slug.type.createdAt.updatedAt
